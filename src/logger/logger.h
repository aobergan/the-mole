/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOGGER_H
#define LOGGER_H 

#include <boost/log/sources/severity_logger.hpp>

#define LOGGER(severity) BOOST_LOG_SEV(TheMoleInstance.getLogger().getLogger(), severity)

enum severity_level {
  debug,
  normal,
  notification,
  warning,
  error,
  critical
};

class Logger {
public:
  typedef boost::log::sources::severity_logger < severity_level > InternalLogger;
  Logger();
  InternalLogger& getLogger();
private:
  InternalLogger mLogger;
};

#include <the_mole.h>


#endif /* LOGGER_H */
