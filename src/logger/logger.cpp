/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "logger.h"

#define BOOST_LOG_USE_NATIVE_SYSLOG 

#include <boost/shared_ptr.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/syslog_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>

namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace attrs = boost::log::attributes;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;

typedef sinks::synchronous_sink< sinks::syslog_backend > sink_t;

Logger::Logger() {
  logging::add_common_attributes();
  logging::core::get()->add_global_attribute("Scope", attrs::named_scope());
  logging::add_console_log(std::clog, keywords::format = "[%TimeStamp%] %ThreadID%: %Message%");
  boost::shared_ptr<sinks::syslog_backend> backend(new sinks::syslog_backend(
    keywords::use_impl = sinks::syslog::native,
    keywords::facility = sinks::syslog::user));
  backend->set_severity_mapper(sinks::syslog::direct_severity_mapping<int>("Severity"));
  boost::shared_ptr<sink_t> sink(new sink_t(backend));
  sink->set_formatter(expr::stream 
    << "[" 
    << expr::format_named_scope("Scope", keywords::format = "%n") 
    << "]:" 
    << expr::message);
  logging::core::get()->add_sink(sink);
}

Logger::InternalLogger& Logger::getLogger() {
  return mLogger;
}

