/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROXY_SERVER_H
#define PROXY_SERVER_H 

#include <string>
#include <memory>
#include <boost/asio/spawn.hpp>
#include <boost/asio/ip/tcp.hpp>

using ServerDataChunkType = std::array<char, 2048>;

class MoleConnection;

class ProxyServer {
public:
  virtual void start_negotiate() = 0;
  virtual void completeNegotiation(boost::asio::ip::address bind_addr, 
      uint16_t bind_port, 
      boost::asio::yield_context yield_context,
      unsigned int result) = 0;
  virtual void sendData(std::shared_ptr<ServerDataChunkType> data, 
      size_t length,
      boost::asio::yield_context yield_context) = 0;
  virtual void receiveData(char *data, size_t &size) = 0;
  virtual void waitForData() = 0;
  virtual void closeSocket() = 0;
  virtual std::string& getDstAddr() = 0;
};

using CreateProxyServerFn = ProxyServer* (*)(std::shared_ptr<MoleConnection>&);

#endif /* PROXY_SERVER_H */
