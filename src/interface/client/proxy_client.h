/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROXY_CLIENT_H
#define PROXY_CLIENT_H 

#include <string>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/spawn.hpp>

using ClientDataChunkType = std::array<char, 2048>;

class MoleConnection;

class ProxyClient {
public:
  virtual void connect(boost::asio::ip::address const addr, uint16_t port, boost::asio::yield_context yield_context) = 0;
  virtual void connect(std::string domain_name, uint16_t port, boost::asio::yield_context yield_context) = 0;
  virtual void waitForData() = 0;
  virtual void sendData(std::shared_ptr<ClientDataChunkType> data, 
      size_t length,
      boost::asio::yield_context yield_context) = 0;
  virtual void closeSocket() = 0;

};

using CreateProxyClientFn = ProxyClient* (*)(std::shared_ptr<MoleConnection> &);

#endif /* PROXY_CLIENT_H */
