/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "the_mole.h"
#include <iostream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>
#include <logger.h>
#include <settings.h>
#include <socks5_server.h>
#include <direct_connection_client.h>

namespace po = boost::program_options;
  
TheMole::TheMole() 
  : m_pLogger(new Logger())
  , m_pStartupSettings(new Settings()) {
}

TheMole& TheMole::getInstance() {
  static TheMole instance;
  return instance;
}

void TheMole::initialize(int argc, char **argv) {
  BOOST_LOG_FUNCTION();

  m_pStartupSettings->addGroup("cmdline", "Command line options");
  m_pStartupSettings->addGroup("cfgfile", "Config file options");

  m_pStartupSettings->addOption<std::string>("cmdline", "config-file,c", "Config file");

  m_pStartupSettings->addOption<std::vector<std::string> >("cfgfile", "outgoing-proxy", "Outgoing proxy server");
  m_pStartupSettings->addOption<int>("cfgfile", "listening-port", "Listening port", 1696);
  m_pStartupSettings->addOption<std::string>("cfgfile", "bind-address", "Bind address", "0.0.0.0");

  try {
    m_pStartupSettings->parseCommandLine(argc, argv, "cmdline");
    std::cout << m_pStartupSettings->getOption<std::string>("config-file") << std::endl;
  } catch(...) {
    m_pStartupSettings->displayGroupOptions("cmdline");
    exit(1);
  }

  try {
    m_pStartupSettings->parseConfigFile(m_pStartupSettings->getOption<std::string>("config-file").c_str(), "cfgfile"); 
    std::vector<std::string> v = m_pStartupSettings->getOption<std::vector<std::string>>("outgoing-proxy");
    std::cout << v[0] << std::endl;
  } catch(...) {
    LOGGER(critical) << "Unable to parse config file";
    exit(1);
  }

  Socks5Server::Register();
  DirectConnectionClient::Register();

  m_uMoleServer.reset(new MoleServer(m_pStartupSettings->getOption<std::string>("bind-address"),
        m_pStartupSettings->getOption<int>("listening-port")));
}

void TheMole::run() {
  m_uMoleServer->run();
  m_uMoleServer->joinAll();
}
