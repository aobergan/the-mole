/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef THE_MOLE_H
#define THE_MOLE_H 

#include <memory>
#include <list>
#include <boost/log/common.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/attributes.hpp>
#include <boost/thread.hpp>
#include <mole_server.h>

#define TheMoleInstance TheMole::getInstance()

class Logger;
class Settings;

//! Main singleton class 
/*! 
    Starts the applications. Contains other classes
*/
class TheMole {
public:
  typedef std::unique_ptr<Logger> LoggerPtr;
  typedef std::unique_ptr<Settings> SettingsPtr;

  static TheMole& getInstance();

  /**
  * @brief Parses the command line
  *
  * @param argc argument from main() function
  * @param argv argument from main() function
  *
  * @return 
  */
  int parseCommandLine(int, char**);
  void initialize(int argc, char **argv);
  void run();
  Logger& getLogger() { return *m_pLogger; }

private:
  std::unique_ptr<MoleServer> m_uMoleServer;
  LoggerPtr m_pLogger;
  SettingsPtr m_pStartupSettings;

private:
  TheMole();
  TheMole(const TheMole&);
  TheMole& operator=(const TheMole&);
};

#endif /* THE_MOLE_H */
