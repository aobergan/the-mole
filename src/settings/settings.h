/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SETTINGS_H
#define SETTINGS_H 

#include <map>
#include <memory>
#include <string>
#include <boost/program_options.hpp>
#include <boost/optional.hpp>
#include <logger.h>

class Settings {
public:
  Settings();
  template <class T> void addOption(const char* group_name
      , const char* name
      , const char* desc
  );
  template <class T> void addOption(const char* group_name
      , const char* name
      , const char* desc
      , T default_value
  );
  template <class T> T getOption(const char* name);
  void displayGroupOptions(const char* group_name);
  void addGroup(const char* group_name, const char* description);
  void parseCommandLine(int argc, char** argv, const char* group_name);
  void parseConfigFile(const char* file_name, const char* group_name);

private:
  std::map<std::string, std::unique_ptr<boost::program_options::options_description>> mGroups;
  boost::program_options::variables_map mVariablesMap;
};

template <class T> void Settings::addOption(const char* group_name
  , const char* name
  , const char* desc
)
{
  BOOST_LOG_FUNCTION();
  try{
      mGroups.at(group_name)->add_options()(name, boost::program_options::value<T>(), desc);
  }
  catch (const std::out_of_range& e) {
    LOGGER(error) << "Attempt to access non existens options group";
  }
}

template <class T> void Settings::addOption(const char* group_name
    , const char* name
    , const char* desc
    , T default_value
) {
  BOOST_LOG_FUNCTION();
  try{
      mGroups.at(group_name)->add_options()(name, boost::program_options::value<T>()->default_value(default_value), desc);
  }
  catch (const std::out_of_range& e) {
    LOGGER(error) << "Attempt to access non existens options group";
  }
}

template <class T> T Settings::getOption(const char* name) {
  return mVariablesMap[name].as<T>(); 
}

#endif /* SETTINGS_H */
