/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "settings.h"
#include <iostream>

namespace po = boost::program_options;

Settings::Settings() 
{ }

void Settings::addGroup(const char* group_name, const char* description)
{
  mGroups.insert(
    std::pair<std::string, std::unique_ptr<po::options_description>> (
      group_name, 
       std::unique_ptr<po::options_description> (
        new po::options_description(description)
      )
    )
  );
}

void Settings::displayGroupOptions(const char* group_name) {
  BOOST_LOG_FUNCTION();
  try {
    std::cerr << *mGroups.at(group_name) << std::endl;
  } catch (const std::out_of_range &e) {
    LOGGER(error) << "Options description group doesn't exist";
  }
}

void Settings::parseCommandLine(int argc, char** argv, const char* group_name) {
  BOOST_LOG_FUNCTION();
  try {
    po::store(po::parse_command_line(argc, argv, *mGroups.at(group_name)), mVariablesMap);
    po::notify(mVariablesMap);
  } catch (std::out_of_range &e) {
    LOGGER(error) << "Options description group doesn't exist";
  }
}

void Settings::parseConfigFile(const char* file_name, const char* group_name) {
  BOOST_LOG_FUNCTION();
  try {
    po::store(po::parse_config_file<char>(file_name, *mGroups.at(group_name)), mVariablesMap);
    po::notify(mVariablesMap);
  } catch (std::out_of_range &e) {
    LOGGER(error) << "Options description group doesn't exist";
  }
}
