/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOCKS5_H
#define SOCKS5_H 

namespace Socks5 {

  typedef struct version_identifier{
    uint8_t ver;
    uint8_t nmethods;
    uint8_t methods[255];
  } __attribute__((packed)) VersionIdentifier;

  typedef struct method_selection {
    uint8_t ver;
    uint8_t method;
  } __attribute__((packed)) MethodSelection;

  typedef struct socks_request {
    uint8_t ver;
    uint8_t cmd;
    uint8_t rsv;
    uint8_t a_typ;
    uint8_t dst_addr[256];
    uint16_t dst_port;
  } __attribute__((packed)) SocksRequest;

  typedef struct socks_reply {
    uint8_t ver;
    uint8_t reply;
    uint8_t rsv;
    uint8_t a_typ;
    uint8_t bind_addr[256];
    uint16_t bind_port;
  } __attribute__((packed)) SocksReply;

} //namespace Socks5

#endif /* SOCKS5_H */
