/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "direct_connection_client.h"
#include <proxy_client_factory.h>
#include <mole_connection.h>
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/lexical_cast.hpp>
#include <logger.h>

DirectConnectionClient::DirectConnectionClient( std::shared_ptr<MoleConnection> &owner ) 
  : m_wOwner(owner)
  , mResolver(owner->getIOService()) {
}

void DirectConnectionClient::connect(boost::asio::ip::address addr, 
    uint16_t port, 
    boost::asio::yield_context yield_context) {
  std::shared_ptr<MoleConnection> connection_handler = m_wOwner.lock();
  boost::system::error_code ec;

  m_uSocket.reset(new boost::asio::ip::tcp::socket(connection_handler->getIOService()));
  m_uSocket->async_connect(boost::asio::ip::tcp::endpoint(addr, port), 
      yield_context[ec]);
  if(!ec) {
    boost::asio::ip::tcp::endpoint ep;
    try {
      ep = m_uSocket->local_endpoint();
    } catch (boost::system::system_error &ec) {
      LOGGER(error) << "Unable to get local endpoint: " << ec.what();
    }
    connection_handler->completeNegotiation(ep.address(), ep.port(), yield_context, 0);
  }
}

void DirectConnectionClient::connect(std::string domain_name, 
    uint16_t port, 
    boost::asio::yield_context yield_context) {
  auto connection_handler = m_wOwner.lock();

  boost::asio::steady_timer timer(connection_handler->getIOService());
  try {
    boost::asio::spawn(connection_handler->getStrand(),
        [this, connection_handler, &timer](boost::asio::yield_context yield_context) {
          boost::system::error_code ec;
          timer.expires_from_now(std::chrono::seconds(2)); 
          timer.async_wait(yield_context[ec]);
          if(timer.expires_from_now() <= std::chrono::seconds(0)) {
            LOGGER(debug) << "Timer expired, canceling resolver";
            mResolver.cancel();
          }
        });
    boost::asio::ip::tcp::resolver::iterator resolver_iterator = mResolver.async_resolve(
      boost::asio::ip::tcp::resolver::query(domain_name, boost::lexical_cast<std::string>(port)), yield_context);
    
    timer.cancel();

    m_uSocket.reset(new boost::asio::ip::tcp::socket(connection_handler->getIOService()));
    boost::system::error_code ec;

    for(boost::asio::ip::tcp::resolver::iterator it; resolver_iterator != it; resolver_iterator++) {
      m_uSocket->async_connect(resolver_iterator->endpoint(), yield_context[ec]);
      if(!ec) {
        boost::asio::ip::tcp::endpoint ep;
        try {
          ep = m_uSocket->local_endpoint();
        } catch (boost::system::system_error &e) {
          LOGGER(error) << "Unable to get local endpoint: " << e.what();
        }
        connection_handler->completeNegotiation(ep.address(), ep.port(),yield_context, 0);
        break;
      }
    }
  } catch (boost::system::system_error &e) {
    LOGGER(error) << "Resolver error: " << e.what() << " " << domain_name;
    timer.cancel();
    boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address_v4(), 0);
    connection_handler->completeNegotiation(ep.address(), ep.port(), yield_context, 0x04);
  }
}

void DirectConnectionClient::waitForData() {
  auto connection_handler = m_wOwner.lock();
    
  boost::asio::spawn(connection_handler->getStrand(),
      [this, connection_handler](boost::asio::yield_context yield_context) {
        boost::system::error_code ec;
        for(;;) {
          std::shared_ptr<ServerDataChunkType> data_chunk(new ServerDataChunkType);
          size_t bytes_read = boost::asio::async_read(*m_uSocket,
            boost::asio::buffer(*data_chunk),
            boost::asio::transfer_at_least(1),
            yield_context[ec]);
          if(!ec) {
            connection_handler->transferDataToServer(data_chunk, bytes_read, yield_context);
          } else {
            if(ec == boost::asio::error::operation_aborted || ec == boost::asio::error::eof) {
              LOGGER(debug) << ec.message();
            } else {
              LOGGER(error) << "DirectConnectionClient error: " << ec.message();
            }
            connection_handler->closeServerSocket();
            break; 
          }
        }
      });
}

void DirectConnectionClient::sendData(std::shared_ptr<ClientDataChunkType> data, 
    size_t length,
    boost::asio::yield_context yield_context) {
  boost::asio::async_write(*m_uSocket,
  boost::asio::buffer(*data, length),
  boost::asio::transfer_all(),
  yield_context);
}

void DirectConnectionClient::closeSocket() {
  boost::system::error_code ec;
  m_uSocket->close(ec);
  mResolver.cancel();
}

ProxyClient* DirectConnectionClient::Create(std::shared_ptr<MoleConnection> &owner) {
  return new DirectConnectionClient(owner);
}

void DirectConnectionClient::Register() {
  ProxyClientFactoryInstance.registerProxyClient("direct" , Create);
}
