/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOCKS5_SERVER_H
#define SOCKS5_SERVER_H 

#include <boost/asio/spawn.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/exception/all.hpp>
#include <proxy_server.h>
#include <proxy_client.h>
#include <socks5.h>
#include <memory>
#include <string>

#define IPV4_SIZE 4
#define IPV6_SIZE 16

class MoleConnection;

struct Socks5ServerException : virtual boost::exception {};
struct Socks5CommandNotSupportedException : virtual boost::exception {};

using error_message = boost::error_info<struct tag_error_message, std::string>;

class Socks5Server : public ProxyServer {
public:
  Socks5Server(std::shared_ptr<MoleConnection> &owner);
  void start_negotiate() override;
  void completeNegotiation(boost::asio::ip::address bind_addr, 
      uint16_t bind_port,
      boost::asio::yield_context yield_context,
      unsigned int result) override;
  void sendData(std::shared_ptr<ClientDataChunkType> data,
      size_t size,
      boost::asio::yield_context yield_context) override;
  void receiveData(char *data, size_t &size) override;
  void waitForData() override;
  void closeSocket() override;
  std::string& getDstAddr() override;
  static ProxyServer* Create(std::shared_ptr<MoleConnection> &owner);
  static void Register();

public:
  static const unsigned char sVersion = 5;

private:
  boost::asio::ip::tcp::socket &mSocket;
  Socks5::VersionIdentifier mVersionIdentifier;
  Socks5::MethodSelection mMethodSelection;
  Socks5::SocksRequest mSocksRequest;
  Socks5::SocksReply mSocksReply;
  std::weak_ptr<MoleConnection> m_wOwner;
};

#endif /* SOCKS5_SERVER_H */

