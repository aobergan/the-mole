/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "socks5_server.h"
#include <proxy_server_factory.h>
#include <mole_connection.h>
#include <logger.h>
#include <algorithm>
#include <array>
#include <cstring>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/completion_condition.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/write.hpp>
#include <boost/exception/get_error_info.hpp>
#include <boost/endian/conversion.hpp>

using boost::asio::yield_context;
using boost::asio::ip::tcp;
using namespace Socks5;

Socks5Server::Socks5Server(std::shared_ptr<MoleConnection> &owner) 
  : m_wOwner(owner)
  , mSocket(owner->getSocket())
{
  std::memset(&mVersionIdentifier, 0, sizeof(VersionIdentifier)); 
  std::memset(&mMethodSelection, 0, sizeof(MethodSelection));
  std::memset(&mSocksRequest, 0, sizeof(SocksRequest));
  std::memset(&mSocksReply, 0, sizeof(SocksReply));
}

void Socks5Server::start_negotiate() {
  auto connection_handler = m_wOwner.lock();
  boost::asio::spawn(connection_handler->getStrand(), 
      [this, connection_handler](boost::asio::yield_context yield_context) {
        try {
          boost::asio::async_read(
              mSocket,
              boost::asio::buffer(&mVersionIdentifier, sizeof(mVersionIdentifier)), 
              boost::asio::transfer_exactly(2),
              yield_context);

          if(mVersionIdentifier.ver != sVersion) { 
            throw Socks5ServerException () 
              << error_message("Invalid Socks header. Closing connection");
          }

          if(mVersionIdentifier.nmethods == 0) {
            throw Socks5ServerException() 
              << error_message("There autnentication method selection should contain at least 1 method");
          }

          boost::asio::async_read(
              mSocket,
              boost::asio::buffer(&mVersionIdentifier.methods, sizeof(mVersionIdentifier.methods)),
              boost::asio::transfer_exactly(mVersionIdentifier.nmethods),
              yield_context);

          mMethodSelection.method = 0xff;
          mMethodSelection.ver = 5;
          std::for_each(mVersionIdentifier.methods, 
              mVersionIdentifier.methods + mVersionIdentifier.nmethods,
              [this](uint8_t method) {
                if(method == 0x00) {
                  mMethodSelection.method = 0x00;
                }
              });

          boost::asio::async_write(mSocket, 
              boost::asio::buffer(&mMethodSelection, sizeof(MethodSelection)),
              boost::asio::transfer_all(),
              yield_context);

          if(mMethodSelection.method == 0xff) {
            throw Socks5ServerException() 
              << error_message("No acceptable methods found");
          }

          boost::asio::async_read(mSocket,
              boost::asio::buffer(&mSocksRequest, sizeof(mSocksRequest)),
              boost::asio::transfer_exactly(4),
              yield_context);

          if(mSocksRequest.ver != 5) {
            throw Socks5ServerException() 
              << error_message("Invalid socks request header");
          }

          mSocksReply.ver = 5;
          mSocksReply.rsv = 0;
          mSocksReply.reply = 0x00;
          mSocksReply.a_typ = mSocksRequest.a_typ;

          if(mSocksRequest.cmd != 0x01) {
            mSocksReply.reply = 0x07;
            throw Socks5CommandNotSupportedException() << error_message("Socks5 command not supported");
          }

          size_t dst_addr_length;

          if(mSocksRequest.a_typ == 0x01) {
            size_t bytes_read = boost::asio::async_read(mSocket,
                std::array<boost::asio::mutable_buffer,2>
                  { boost::asio::buffer(mSocksRequest.dst_addr, IPV4_SIZE),
                    boost::asio::buffer(&mSocksRequest.dst_port, sizeof(mSocksRequest.dst_port)) },
                boost::asio::transfer_all(),
                yield_context);

            dst_addr_length = IPV4_SIZE;
            boost::asio::ip::address ip4addr(boost::asio::ip::address_v4(
                  boost::endian::big_to_native(*((uint32_t*)&mSocksRequest.dst_addr[0]))));
            connection_handler->setupOutgoingConnection(ip4addr, 
                boost::endian::big_to_native(mSocksRequest.dst_port),
                yield_context);

          } else if(mSocksRequest.a_typ == 0x04) {
            boost::asio::async_read(mSocket,
                std::array<boost::asio::mutable_buffer,2>
                  { boost::asio::buffer(mSocksRequest.dst_addr, IPV6_SIZE),
                    boost::asio::buffer(&mSocksRequest.dst_port, sizeof(mSocksRequest.dst_port)) },
                boost::asio::transfer_all(),
                yield_context);
            dst_addr_length = IPV6_SIZE;

          } else if(mSocksRequest.a_typ == 0x03) {
            boost::asio::async_read(mSocket,
                boost::asio::buffer(mSocksRequest.dst_addr, 1),
                boost::asio::transfer_all(),
                yield_context);

            boost::asio::async_read(mSocket, 
                std::array<boost::asio::mutable_buffer, 2>
                  { boost::asio::buffer(&mSocksRequest.dst_addr[1], mSocksRequest.dst_addr[0]),
                    boost::asio::buffer(&mSocksRequest.dst_port, sizeof(mSocksRequest.dst_port)) },
                boost::asio::transfer_all(),
                yield_context);

            dst_addr_length = mSocksRequest.dst_addr[0];
            std::string domain_name(&mSocksRequest.dst_addr[1], &mSocksRequest.dst_addr[1] + dst_addr_length);
            connection_handler->setupOutgoingConnection(domain_name, 
                boost::endian::big_to_native(mSocksRequest.dst_port),
                yield_context);
          }

        } catch(Socks5ServerException &e) {
          if(std::string const *msg = boost::get_error_info<error_message>(e)) {
            LOGGER(error) << *msg;
          }
        } catch(Socks5CommandNotSupportedException &e) {
          if(std::string const *msg = boost::get_error_info<error_message>(e)) {
            LOGGER(error) << *msg;
          }
          boost::system::error_code ec;
          boost::asio::async_write(mSocket,
              std::array<boost::asio::const_buffer, 3> {
                boost::asio::buffer(&mSocksReply, 4),
                boost::asio::buffer(&mSocksReply.bind_addr, 4),
                boost::asio::buffer(&mSocksReply.bind_port, sizeof(mSocksReply.bind_port)) },
              boost::asio::transfer_all(),
              yield_context[ec]);
          if(ec) {
            LOGGER(error) << "Socket write error";
          }

        } catch (const boost::system::system_error &e) {
          LOGGER(error) << "Socket error: " << e.what();
        }
      });
}

void Socks5Server::completeNegotiation(boost::asio::ip::address bind_addr, 
    uint16_t bind_port,
    boost::asio::yield_context yield_context,
    unsigned int result) {
  auto connection_handler = m_wOwner.lock();
  mSocksReply.reply = result;
  if(bind_addr.is_v4()) {
    mSocksReply.a_typ = 0x01;
    boost::asio::ip::address_v4::bytes_type tmp = bind_addr.to_v4().to_bytes();
    std::copy(tmp.begin(), tmp.end(), &mSocksReply.bind_addr[0]);
    mSocksReply.bind_port = boost::endian::native_to_big(bind_port);

    boost::asio::async_write(mSocket, 
        std::array<boost::asio::const_buffer, 3> {
          boost::asio::buffer(&mSocksReply, 4),
          boost::asio::buffer(mSocksReply.bind_addr, IPV4_SIZE),
          boost::asio::buffer(&mSocksReply.bind_port, sizeof(mSocksReply.bind_port)) 
        },
        [this, connection_handler, result](boost::system::system_error ec, size_t) { 
          if(!result) {
            connection_handler->startSession(); 
          }
        });
  } else if(bind_addr.is_v6()) {
    // Do nothing, while ipv6 is not supported yet
    // TODO: Add ipv6 support
  }
}

std::string& Socks5Server::getDstAddr()
{
}

void Socks5Server::receiveData(char *data, size_t &size)
{
}

void Socks5Server::sendData(std::shared_ptr<ClientDataChunkType> data, 
    size_t length,
    boost::asio::yield_context yield_context) 
{
  boost::system::error_code ec;
  boost::asio::async_write(mSocket,
    boost::asio::buffer(*data, length),
    boost::asio::transfer_all(),
    yield_context[ec]);
  if(ec) {
    LOGGER(error) << "Socket write error";
  }
}

ProxyServer* Socks5Server::Create(std::shared_ptr<MoleConnection> &owner) {
  return new Socks5Server(owner);
}  

void Socks5Server::Register()
{
  ProxyServerFactoryInstance.registerProxyServer("socks5", Create);
}

void Socks5Server::waitForData() {
  auto connection_handler = m_wOwner.lock();
    
  boost::asio::spawn(connection_handler->getStrand(),
      [this, connection_handler](boost::asio::yield_context yield_context) {
        boost::system::error_code ec;
        for(;;) {
          std::shared_ptr<ClientDataChunkType> data_chunk(new ClientDataChunkType);
          size_t bytes_read = mSocket.async_read_some(boost::asio::buffer(*data_chunk),
              yield_context[ec]);
          if(!ec) {
            connection_handler->transferDataToClient(data_chunk, bytes_read, yield_context);
          } else {
            if(ec == boost::asio::error::operation_aborted || ec == boost::asio::error::eof) {
              LOGGER(debug) << ec.message();
            } else {
              LOGGER(error) << "Socks5Server error: " << ec.message();
            }
            connection_handler->closeClientSocket();
            break; 
          }
        }
      });
}

void Socks5Server::closeSocket() {
  boost::system::error_code ec;
  mSocket.close(ec);
}
