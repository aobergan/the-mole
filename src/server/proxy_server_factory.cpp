/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "proxy_server_factory.h"
#include <utility>

ProxyServerFactory& ProxyServerFactory::getInstance() 
{
  static ProxyServerFactory instance;
  return instance;
}

void ProxyServerFactory::registerProxyServer(std::string const &name, CreateProxyServerFn create_function)
{
  mCreateFunctions.insert(std::make_pair(name, create_function));
}

ProxyServer* ProxyServerFactory::Create(std::string const &name,
    std::shared_ptr<MoleConnection> &owner)
{
  return mCreateFunctions[name](owner);
}
