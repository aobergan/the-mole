/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mole_server.h"
#include "mole_connection.h"
#include <logger.h>
#include <boost/asio/ip/address.hpp>
#include <boost/lexical_cast.hpp>
#include <pthread.h>

using boost::asio::ip::tcp;
using boost::asio::ip::address;

MoleServer::MoleServer(const std::string& bind_addr, uint16_t port) 
  : mAcceptor(mIOService)
  , mCurrentIOService(0)
{ 
  LOGGER(debug) << "Starting listener";

  tcp::endpoint local_ep(address::from_string(bind_addr), port);
  mAcceptor.open(local_ep.protocol());
  mAcceptor.set_option(tcp::acceptor::reuse_address(true));
  mAcceptor.bind(local_ep);
  mAcceptor.listen();

  boost::asio::spawn(mIOService,
      [&](boost::asio::yield_context yield)
      {
        for(;;) {
          boost::system::error_code ec;
          boost::asio::io_service &io = mIOService;
          tcp::socket socket(io);
          mAcceptor.async_accept(socket, yield[ec]);
          if(!ec) std::make_shared<MoleConnection>(std::move(socket), io)->run();
        }
      });
}

boost::asio::io_service& MoleServer::getNextIOService() {
  return *mWorkingIOServices[mCurrentIOService++ % mWorkingIOServices.size()];
}

void MoleServer::run() {
  for(int i = 0; i < 2; i++) {
    std::thread thread([this]() { mIOService.run(); });
    std::string thread_name = "IOService_" + boost::lexical_cast<std::string>(i);
    LOGGER(debug) << "pthread_setname_np result: " << pthread_setname_np(thread.native_handle(), thread_name.c_str());
    mThreads.push_back(std::move(thread));
  }
}

void MoleServer::joinAll() {
  std::for_each(mThreads.begin(), mThreads.end(), [](std::thread &thread) { thread.join(); });
}
