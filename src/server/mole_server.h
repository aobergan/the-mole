/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOLE_SERVER_H
#define MOLE_SERVER_H 

#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <vector>
#include <memory>
#include <thread>

class MoleServer {
public:
  MoleServer(const std::string& bind_addr, uint16_t port);
  MoleServer(const MoleServer&) = delete;
  MoleServer& operator=(const MoleServer&) = delete;
  boost::asio::io_service& getNextIOService();
  void run();
  void joinAll();

private:
  boost::asio::io_service mIOService;
  boost::asio::ip::tcp::acceptor mAcceptor;
  std::vector<std::unique_ptr<boost::asio::io_service>> mWorkingIOServices;
  std::vector<std::unique_ptr<boost::asio::io_service::work>> mWorks;
  std::vector<std::thread> mThreads;
  unsigned int mCurrentIOService;
};

#endif /* MOLE_SERVER_H */
