/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROXY_SERVER_FACTORY_H
#define PROXY_SERVER_FACTORY_H 

#define ProxyServerFactoryInstance ProxyServerFactory::getInstance()

#include <proxy_server.h>
#include <string>

class ProxyServerFactory {
public:
  void registerProxyServer(std::string const &name, CreateProxyServerFn create_function); 
  ProxyServer* Create(std::string const &name, 
      std::shared_ptr<MoleConnection> &owner);
  static ProxyServerFactory& getInstance();

private:
  std::map<std::string, CreateProxyServerFn> mCreateFunctions;

private:
  ProxyServerFactory(){}
  ProxyServerFactory(const ProxyServerFactory&) {}
  ProxyServerFactory& operator=(const ProxyServerFactory&) { return *this; }
};

#endif /* PROXY_SERVER_FACTORY_H */
