/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mole_connection.h"
#include "mole_server.h"
#include <proxy_server_factory.h>
#include <proxy_client_factory.h>
#include <logger.h>

MoleConnection::MoleConnection(boost::asio::ip::tcp::socket socket, boost::asio::io_service &io_service) 
  : mSocket(std::move(socket))
  , mStrand(io_service) 
  , mIOService(io_service) {
  sInstances++;
}

MoleConnection::~MoleConnection() {
  sInstances--;
  LOGGER(debug) << "Destroying connection object";
  LOGGER(debug) << "Connections left: " << sInstances;
}

void MoleConnection::run() {
  auto connection_handler(shared_from_this());
  m_uProxyServer.reset(ProxyServerFactoryInstance.Create("socks5", connection_handler));
  m_uProxyClient.reset(ProxyClientFactoryInstance.Create("direct", connection_handler));
  m_uProxyServer->start_negotiate();
}

void MoleConnection::setupOutgoingConnection(const boost::asio::ip::address &addr, 
    uint16_t port, 
    boost::asio::yield_context yield_context) {
  LOGGER(debug) << "Creating outgoing connection to addr " << addr << ", port: " << port;
  m_uProxyClient->connect(addr, port, yield_context);
}

void MoleConnection::setupOutgoingConnection(const std::string &domain_name, 
    uint16_t port,
    boost::asio::yield_context yield_context) {
  m_uProxyClient->connect(domain_name, port, yield_context);
}

void MoleConnection::completeNegotiation(const boost::asio::ip::address &bind_addr, 
    uint16_t bind_port,
    boost::asio::yield_context yield_context,
    unsigned int result) {
  m_uProxyServer->completeNegotiation(bind_addr, bind_port, yield_context, result);
}

void MoleConnection::transferDataToClient(std::shared_ptr<ClientDataChunkType> data, 
    size_t length,
    boost::asio::yield_context &yield_context) {
  m_uProxyClient->sendData(data, length, yield_context);
}

void MoleConnection::transferDataToServer(std::shared_ptr<ServerDataChunkType> data, 
    size_t length,
    boost::asio::yield_context &yield_context) {
  m_uProxyServer->sendData(data, length, yield_context);
}

void MoleConnection::startSession() {
  m_uProxyClient->waitForData();
  m_uProxyServer->waitForData();
}

void MoleConnection::closeClientSocket() {
  m_uProxyClient->closeSocket();
}

void MoleConnection::closeServerSocket() {
  m_uProxyServer->closeSocket();
}

std::atomic<unsigned int> MoleConnection::sInstances(0);
