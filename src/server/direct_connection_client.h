/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIRECT_CONNECTION_CLIENT_H
#define DIRECT_CONNECTION_CLIENT_H 

#include <proxy_client.h>
#include <proxy_server.h>
#include <memory>
#include <boost/asio/ip/tcp.hpp>

class DirectConnectionClient : public ProxyClient {
public:
  DirectConnectionClient(std::shared_ptr<MoleConnection> &owner);
  void connect(boost::asio::ip::address addr, 
      uint16_t port, 
      boost::asio::yield_context yield_context) override;
  void connect(std::string domain_name, 
      uint16_t port, 
      boost::asio::yield_context yield_context) override;
  void waitForData() override;
  void sendData(std::shared_ptr<ClientDataChunkType> data, 
      size_t length,
      boost::asio::yield_context yield_context) override;
  void closeSocket() override;
  static ProxyClient* Create( std::shared_ptr<MoleConnection> &owner);
  static void Register();
  
private:
  std::weak_ptr<MoleConnection> m_wOwner;
  std::unique_ptr<boost::asio::ip::tcp::socket> m_uSocket; 
  boost::asio::ip::tcp::resolver mResolver;
};

#endif /* DIRECT_CONNECTION_CLIENT_H */
