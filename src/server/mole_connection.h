/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOLE_CONNECTION_H
#define MOLE_CONNECTION_H 

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/write.hpp>
#include <proxy_server.h>
#include <proxy_client.h>
#include <atomic>
#include <memory>

class MoleConnection : public std::enable_shared_from_this<MoleConnection> {
public:
  explicit MoleConnection(boost::asio::ip::tcp::socket socket, boost::asio::io_service &io_service);
  ~MoleConnection();
  void run();
  boost::asio::ip::tcp::socket& getSocket() { return mSocket; }
  boost::asio::io_service::strand& getStrand() { return mStrand; }
  boost::asio::io_service &getIOService() { return mIOService; }
  void setupOutgoingConnection(boost::asio::ip::address const &addr, 
      uint16_t port,
      boost::asio::yield_context yield_context);
  void setupOutgoingConnection(std::string const &domain_name, 
      uint16_t port,
      boost::asio::yield_context yield_context);
  void completeNegotiation(const boost::asio::ip::address &bind_addr, 
      uint16_t bind_port,
      boost::asio::yield_context yield_context,
      unsigned int result);
  void transferDataToClient(std::shared_ptr<ClientDataChunkType> data, 
      size_t length, 
      boost::asio::yield_context &yield_context);
  void transferDataToServer(std::shared_ptr<ServerDataChunkType> data, 
      size_t length, 
      boost::asio::yield_context &yield_context);
  void closeClientSocket();
  void closeServerSocket();
  void startSession();
private:
  boost::asio::ip::tcp::socket mSocket;
  boost::asio::io_service::strand mStrand;
  std::unique_ptr<ProxyServer> m_uProxyServer;
  std::unique_ptr<ProxyClient> m_uProxyClient;
  boost::asio::io_service &mIOService;
  static std::atomic<unsigned int> sInstances;
};

#endif /* MOLE_CONNECTION_H */
