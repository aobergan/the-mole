/*
*    The Mole - distributes the load betweed several proxies
*
*    Copyright (C) 2015 Alexey Obergan
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROXY_CLIENT_FACTORY_H
#define PROXY_CLIENT_FACTORY_H 

#include <proxy_client.h>
#include <map>
#include <string>

#define ProxyClientFactoryInstance ProxyClientFactory::getInstance()

class ProxyClientFactory {
public:
  void registerProxyClient(std::string const &name, CreateProxyClientFn create_function);
  ProxyClient* Create(std::string const &name, 
      std::shared_ptr<MoleConnection> &owner);
  static ProxyClientFactory& getInstance();

private:
  std::map<std::string, CreateProxyClientFn> mCreateFunctions;

private:
  ProxyClientFactory() {}
  ProxyClientFactory(const ProxyClientFactory&) {}
  ProxyClientFactory& operator=(const ProxyClientFactory&) { return *this; }
};

#endif /* PROXY_CLIENT_FACTORY_H */
